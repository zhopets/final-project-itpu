const { src, dest } = require('gulp');
const concat = require('gulp-concat');

const cssBundle = () =>
  src([
    'src/dist/style.css',
    'src/dist/mobile.css',
  ])
  .pipe(concat('bundle.css'))
  .pipe(dest('src/dist'));

  exports.cssBundle = cssBundle;
